
import './App.css';
import Banner from './ex-1/Banner';
import Footer from './ex-1/Footer';
import Header from './ex-1/Header';
import Item from './ex-1/Item';

function App() {
  return (
    <div className="App">
      <Header/>
      <div className="py-5">
      <Banner/>
      </div>
      <div className="mt-5">
        <Item/>
      </div>
      <div className="mt-5">
        <Footer/>
      </div>
 
  
    </div>
  );
}

export default App;
