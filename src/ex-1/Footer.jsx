import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    return (
      <div>
        <div className="bg-dark p-5">
        <div className="container px-lg-5 ">
        <p className="text-white">
        Copyright © Your Website 2022
        </p>
        </div>
      </div>
      </div>
    )
  }
}
