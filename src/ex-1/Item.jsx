import React, { Component } from 'react'

export default class Item extends Component {
  render() {
    return (
      <div>
        <div className="container px-lg-5">
        <div className="row gx-lg-5">
            
    <div className="col-lg-6 col-xxl-4 mb-5">
        <div className="card bg-light border-0 h-100">
  <div className="card-body p-4 p-lg-5 pt-0 pt-lg-0 bg-light h-50 position-relative">
  <a href="#" className='text-white bg-primary pt-2 pb-2 p-3 rounded-3 mt-n4 position-absolute translate-middle'>
  <i class="bi bi-collection fs-3"></i>
  </a>
    <h5 className="card-title fw-bold mt-5  pb-2">Fresh new layout</h5>
    <p className="card-text lh-sm">With Bootstrap 5, we've created a fresh new layout for this template!</p>
    </div>
    </div>
    </div>
    <div className="col-lg-6 col-xxl-4 mb-5">
        <div className="card bg-light border-0 h-100">
  <div className="card-body p-4 p-lg-5 pt-0 pt-lg-0 bg-light h-50 position-relative">
  <a href="#" className='text-white bg-primary pt-2 pb-2 p-3 rounded-3 mt-n4 position-absolute translate-middle'>
  <i class="bi bi-cloud-download fs-3"></i>
  </a>
    <h5 className="card-title fw-bold mt-5  pb-2">Free to download</h5>
    <p className="card-text lh-sm">As always, Start Bootstrap has a powerful collectin of free templates.</p>
    </div>
    </div>
    </div>
    <div className="col-lg-6 col-xxl-4 mb-5">
        <div className="card bg-light border-0 h-100">
  <div className="card-body p-4 p-lg-5 pt-0 pt-lg-0 bg-light h-50 position-relative">
  <a href="#" className='text-white bg-primary pt-2 pb-2 p-3 rounded-3 mt-n4 position-absolute translate-middle'>
  <i class="bi bi-card-heading fs-3"></i>
  </a>
    <h5 className="card-title fw-bold mt-5  pb-2">Jumbotron hero header</h5>
    <p className="card-text lh-sm">The heroic part of this template is the jumbotron hero header!</p>
    </div>
    </div>
    </div>
    <div className="col-lg-6 col-xxl-4 mb-5">
        <div className="card bg-light border-0 h-100">
  <div className="card-body p-4 p-lg-5 pt-0 pt-lg-0 bg-light h-50 position-relative">
  <a href="#" className='text-white bg-primary pt-2 pb-2 p-3 rounded-3 mt-n4 position-absolute translate-middle'>
  <i class="bi bi-bootstrap fs-3"></i>
  </a>
    <h5 className="card-title fw-bold mt-5  pb-2">Feature boxes</h5>
    <p className="card-text lh-sm">We've created some custom feature boxes using Bootstrap icons!</p>
    </div>
    </div>
    </div>
    <div className="col-lg-6 col-xxl-4 mb-5">
        <div className="card bg-light border-0 h-100">
  <div className="card-body p-4 p-lg-5 pt-0 pt-lg-0 bg-light h-50 position-relative">
  <a href="#" className='text-white bg-primary pt-2 pb-2 p-3 rounded-3 mt-n4 position-absolute translate-middle'>
  <i class="bi bi-code fs-3"></i>
  </a>
    <h5 className="card-title fw-bold mt-5  pb-2">Simple clean code</h5>
    <p className="card-text lh-sm">With Bootstrap 5, we've created a fresh new layout for this template!.</p>
    </div>
    </div>
    </div>
    <div className="col-lg-6 col-xxl-4 mb-5">
        <div className="card bg-light border-0 h-100">
  <div className="card-body p-4 p-lg-5 pt-0 pt-lg-0 bg-light h-50 position-relative">
  <a href="#" className='text-white bg-primary pt-2 pb-2 p-3 rounded-3 mt-n4 position-absolute translate-middle'>
  <i class="bi bi-patch-check fs-3"></i>
  </a>
    <h5 className="card-title fw-bold mt-5  pb-2">A name you trust</h5>
    <p className="card-text lh-sm">Start Bootstrap has been the leader in free Bootstrap templates since 2013!</p>
    </div>
    </div>
    </div>
    
    

   
    </div>


    </div>
    </div>

    )
  }
}
