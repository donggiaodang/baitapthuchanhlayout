import React, { Component } from 'react'

export default class Header extends Component {
  render() {
    return (
      <div className=''>
   <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
    <div className="container px-lg-5">
  <a className="navbar-brand " href="#">Start bootstrap</a>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon" />
  </button>
  <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul className="navbar-nav ms-auto ">
      <li className="nav-item active">
        <a className="nav-link active" href="#">Home <span className="sr-only active"></span></a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="#">About</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="#">Contact</a>
      </li>
    </ul>

  </div>
  </div>
</nav>

        </div>
    )
  }
}
